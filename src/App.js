import React, {useState, useEffect} from 'react';
import UsersAutocompleteList from "./app/Users/components/UsersAutocompleteList";
import UsersSearch from './app/Users/components/UsersSearch'
import {Provider} from 'react-redux'
import store from './store'


const App = () => {

    return (
        <Provider store={store}>
            <div >
                <UsersSearch/>
                <UsersAutocompleteList/>
            </div>
        </Provider>
    );
}

export default App;


