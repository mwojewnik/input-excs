import types from "./types";
import axios from "axios";

const setAutocompleteList = userList => ({type: types.SET_AUTOCOMPLETE_LIST, data: userList})

const setSearchText = text => ({type: types.SET_SEARCH_TEXT, text})

const search = text => (async (dispatch, getState) => {
    const response = await axios('https://jsonplaceholder.typicode.com/users')

    const filtered = response.data.filter((item) =>
        item.name.toLowerCase().includes(text.toLowerCase())
    )

    if (text.length === 0) {
        dispatch(setAutocompleteList([]))
    } else {
        dispatch(setAutocompleteList(filtered))
    }


})

const clearList = userList => ({type: types.CLEAR_LIST, data: userList})

export default {
    setAutocompleteList,
    search,
    setSearchText,
    clearList
}
