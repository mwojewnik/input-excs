import types from './types'


const INITIAL_STATE = {
    listName: 'Users',
    data: []
}

const users = (state = INITIAL_STATE, action) => {

    switch (action.type) {
        case types.SET_AUTOCOMPLETE_LIST:
            return {...state, data: action.data};
        case types.SEARCH_USER:
            return {...state, data: state.data}
        case types.SET_SEARCH_TEXT :
            return {...state, text: action.text}
        case types.CLEAR_LIST:
            return{...state, data: action.data}
        default:
            return state
    }
}

export default users;
