import React from 'react';
import {useSelector, useDispatch} from 'react-redux'
import actions from '../duck/actions'
import style from './UsersAutocompleteListStyles.css'

const UsersAutocompleteList = () => {
    const dispatch = useDispatch()
    const users = useSelector(state => state.users.data) || []

    const onClick = (text) => {

                dispatch(actions.setSearchText(text))
                dispatch(actions.clearList(actions.data))


    }

    return (
        <>
            {users.map((user, i) => (
                <table key={i}>
                    <tbody>
                    <tr onClick={() => onClick(user.name)}>
                        <td>{user.name}</td>
                    </tr>
                    </tbody>
                </table>
            ))}
        </>
    )
}

export default UsersAutocompleteList

/*const mapStateToProps = state => {
    console.log('connect', state)
    return {
        users: state.users

}

}

export default connect(mapStateToProps, null)(UsersAutocompleteList)*/


