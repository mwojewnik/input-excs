import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import actions from "../duck/actions";

const UsersSearch = (props) => {
    const dispatch = useDispatch()
    const text = useSelector(state => state.users.text) || ''

    const handleSubmit = () => {
        if(text){
            alert('Użytkownik: ' + text)
        }else{
            alert('Wybierz użytkownika ')
        }

    }

    const searchUser = (e) => {
        dispatch(actions.setSearchText(e.target.value))
        dispatch(actions.search(e.target.value))



    }

    return <>
     {/*   <p>{text}</p>*/}
        <input value={text} onChange={searchUser}/>
        <button onClick={handleSubmit}>Submit</button>
    </>

}

export default UsersSearch;
/*
const mapDispatchToProps = dispatch => {
    return (
        {search: item => dispatch(actions.search(item))}
    )
}

export default connect(null, mapDispatchToProps)(UsersSearch)*/
