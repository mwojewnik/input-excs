const assert = (value, expected, msg) => {
    if (value === expected) {
        console.warn('OK  ', msg);
    } else {
        console.warn('FAIL', msg, 'value =', value, 'expected =', expected);
    }
}

// 1. Please write a function that shows the usage of closures
const next = (function () {
    let counter = 0;
    return function () {
        return ++counter
    }
})();

assert(next(), 1, ' Next should be equal 1')
assert(next(), 2, ' Next should be equal 2')
assert(next(), 3, ' Next should be equal 3')


// 2. Please write a function that returns a sum of array items
// example input [9, 1, 22, 0, 2]
// example output 34
const numbers = [9, 1, 22, 0, 2];

const sum = (numbers) => {
    let result = 0;
    if (numbers) {
        for (const n of numbers) {
            result += n;
        }
    }
    return result
}


assert(sum(null), 0, `Sum of null array should equal 0`)
assert(sum(undefined), 0, `Sum of null array should equal 0`)
assert(sum([-1]), -1, `Sum of [-1] should equal -1`)
assert(sum(numbers), 34, `Sum of ${numbers} should equal 34`)


// 3. Please write a recursive function that flattens a list of items
// example input [[2, [4, [44,5,6]]], [4,5,6], [[2,4], 4], 5]]
// example output [2, 4, 44, 5, 6, 4, 5, 6, 2, 4, 4, 5]

const arr = [[2, [4, [44, 5, 6]]]]
const arr2 = [4, 5, 6]
const arr3 = [[[2, 4], 4], 5]

const myArr = (a, b, c) => {
    if (a && b && c) {
        return a.flat(3).concat(b.flat().concat(c.flat(2)))
    }


}

console.log(myArr(arr, arr2, arr3), 'should be [2, 4, 44, 5, 6, 4, 5, 6, 2, 4, 4, 5] ')
console.log(myArr(null, arr2, arr3), 'should be undefined ')
console.log(myArr(arr, undefined, arr3), 'should be undefined ')

// 4. Please write a function that finds all common elements of two arrays(only primitive types as array elements, order doesn't matter)
// example inputs ['b', 3, 4, 76, 'c'], ['a', 'b', 4, 76, 21, 'e']
// example output ['b', 4, 76]


const commonElements = (a, b) => {
    if (a && b) {
        return a.filter(val => b.includes(val))
    }

}


console.log(commonElements(['b', 3, 4, 76, 'c'], ['a', 'b', 4, 76, 21, 'e']), `Should be = ['b', 4, 76] `)
console.log(commonElements(undefined, [4, 5, 6, 7]), `Should be = undefined `)
console.log(commonElements(null, [4, 5, 6, 7]), `Should be = undefined `)


// 5. Please write a function that finds all different elements of two arrays(only primitive types as array elements, order doesn't matter)
// example inputs ['b', 3, 4, 76, 'c'], ['a', 'b', 4, 76, 21, 'e']
// example output ['a', 3, 21, 'c', 'e']


const filterDif = (a  , b ) => {
    if (a  && b ) {
        let ab = b.filter(val => !a.find(val2 => val === val2));
        let ab2 = a.filter(val => !b.find(val2 => val === val2));
        const differences = ab.concat(ab2)
        return differences
    }
}

console.log(filterDif(['b', 3, 4, 76, 'c'], ['a', 'b', 4, 76, 21, 'e']), `Should be = ['a', 3, 21, 'c', 'e']`)
console.log(filterDif(undefined, ['a', 'b', 4, 76, 21, 'e']), `Should be = undefined`)
console.log(filterDif(null, ['a', 'b', 4, 76, 21, 'e']), `Should be = undefined`)



// 6. Please write a function that takes two arrays of items and returns an array of tuples made from two input arrays at the same indexes. Excessive items should be dropped.
// example input [1,2,3], [4,5,6,7]
// example output [[1,4], [2,5], [3,6]]
const zip = (array1 = [], array2 = []) => {
    let newArray = [];
    const size = Math.min(array1.length, array2.length)
    for (let i = 0; i < size; i++) {
        newArray.push([array1[i], array2[i]])
    }
    return newArray
}
console.log(zip([1, 2, 3], [4, 5, 6, 7]), `Should be = [[1,4], [2,5], [3,6]]`)
console.log(zip([1, 2, 3], [4, 5]), `Should be = [[1,4], [2,5]]`)

// 7. Please write a function which takes a path(path is an array of keys) and object, then returns value at this path. If value at path doesn't exists, return undefined.
// example inputs ['a', 'b', 'c', 'd'], { a: { b: { c: { d: '23' } } } }
// example output '23'

const get = (obj, path = []) => {
    if (obj && path.length > 0) {
        const key = path.shift();
        return get(obj[key], path)
    }
    return obj;
}
assert(get({a: {b: {c: {d: '23'}}}}, ['a', 'b', 'c', 'd']), '23', 'Should be 23')
assert(get({a: {b: {c: {d: '23'}}}}, ['a', 'b', 'c', 'e']), undefined, 'Should be undefined')
assert(get({a: {b: {c: {d: '23'}}}}, ['c', 'b', 'c', 'd']), undefined, 'Should be undefined')

// 8. Please write compare function which compares 2 objects for equality.
// example input { a: 'b', c: 'd' }, { c: 'd', a: 'b' }  /// output true
// example input { a: 'c', c: 'a' }, { c: 'd', a: 'b', q: 's' }  /// output false

const equals = (a, b) => {
    if (a === b) return true;
    const keys = Object.keys(a);
    if (keys.length !== Object.keys(b).length) return false;
    return keys.every(k => equals(a[k], b[k]));
};

const obj1 = {a: 'b', c: 'd'}
const obj2 = {c: 'd', a: 'b'}


assert(equals({a: 'b', c: 'd'}, {c: 'd', a: 'b'}), true)
assert(equals({a: 'c', c: 'a'}, {c: 'd', a: 'b', q: 's'}), false)

//assert(equals(undefined, { c: 'd', a: 'b' }), undefined)


// 9. Please write a function which takes a list of keys and an object, then returns this object, just without keys from the list
// example input ['color', 'size'], { color: 'Blue', id: '22', size: 'xl' }
// example output { id: '22' }
const removeKeys = (obj, keys) => {
    const newObj = Object.assign({}, obj)
    console.log('NEWW', newObj)
    if (keys) {
        for (const k of keys) {
            delete newObj[k]
        }
    }
    return newObj
}
console.log(removeKeys({color: 'Blue', id: '22', size: 'xl'}, ['color', 'size']), `Should be = { id: '22' }`)
