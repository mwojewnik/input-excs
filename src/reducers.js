import {combineReducers} from "redux";
import users from './app/Users/duck'

const rootReducer = combineReducers({users})

export default rootReducer;
